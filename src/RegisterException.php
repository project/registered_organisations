<?php

namespace Drupal\registered_organisations;

/**
 * An exception thrown when issues are encountered with the
 * register itself.
 */
class RegisterException extends \Exception {

}
