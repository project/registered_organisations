<?php

namespace Drupal\registered_organisations;

use Psr\Http\Message\ResponseInterface;

/**
 * An exception thrown when issues are found with the data retrieved
 * from a register.
 */
class TemporaryException extends \Exception {

  /**
   * The expected length of time needed for a reattempt to become successful.
   */
  protected int $retry = 600;

  public function __construct(string $message, int $code, int $retry = 600) {
    $this->retry = $retry;

    parent::__construct($message, $code);
  }

}
