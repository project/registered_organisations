<?php

namespace Drupal\registered_organisations\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an organisation register plugin annotation object.
 *
 * @see \Drupal\registered_organisations\OrganisationManager
 * @see plugin_api
 *
 * @Annotation
 */
class OrganisationRegister extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * The organisation's 'home page' url.
   *
   * @var string
   */
  public $url;

  /**
   * The organisation's 'registry search page' url.
   *
   * @var string
   */
  public $search_url;

}
