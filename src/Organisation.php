<?php

namespace Drupal\registered_organisations;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;

/**
 * A profile class for structuring organisation information.
 */
class Organisation implements OrganisationInterface {

  /**
   * The register plugin id where this company was found.
   *
   * @var string
   */
  protected string $register;

  /**
   * The organisation id.
   *
   * The id for organisation in the register.
   *
   * @var string
   */
  protected string $id;

  /**
   * The organisation name.
   *
   * @var string
   */
  protected string $name;

  /**
   * The organisation type.
   *
   * @var string
   */
  protected string $type;

  /**
   * The organisation status.
   *
   * @var string
   */
  protected string $status;

  /**
   * The organisation's industry classification.
   *
   * This must contain two keys:
   *   - 'codes': (array) of industry classification codes
   *   - 'system': (string) representing the industry classification system,
   *      one of SIC, NACE, NAICS
   *
   * @var array
   */
  protected array $classification = [];

  /**
   * The raw organisation data.
   *
   * An array with the following keys is expected:
   * [
   *   'id': string,
   *   'name': string,
   *   'type': string,
   *   'status': string,
   *   'classification': array ['codes': array, 'system': string]
   * ]
   *
   * @var array
   */
  protected array $data = [];

  /**
   * Constructs a \Drupal\Component\Plugin\PluginBase object.
   *
   * @param OrganisationRegisterInterface $register
   *   The register that holds information about this organisation.
   * @param array $data
   *   An array of raw data as provided by the organisation register.
   *
   * @throws DataException
   *   In the event that mandatory data is missing.
   */
  public function __construct(OrganisationRegisterInterface $register, array $data) {
    $this->register = $register->getPluginId();
    $this->data = $data;

    // Ensure that all mandatory data is set.
    if (!isset($data['id']) || !isset($data['name'])) {
      throw new DataException('The organisation must have a name and a registered number.');
    }

    // Populate this object with the data.
    $this->hydrate();
  }

  /**
   * Serialize with only the raw data.
   *
   * @return string[]
   */
  public function __sleep() {
    return array('registry', 'data');
  }

  /**
   * Repopulate all the data.
   *
   * @return void
   */
  public function __wakeup() {
    $this->hydrate();
  }

  /**
   * Get the organisation register.
   */
  public function getOrganisationManager() {
    return \Drupal::service('registered_organisations.organisation_manager');
  }

  /**
   * Hydrate the object by passing data from an array.
   */
  public function hydrate() {
    foreach ($this->data as $prop => $elem) {
      // Skip empty properties.
      if (!$elem) {
        continue;
      }

      // Set properties on this object.
      $set_method_name = 'set' . ucwords(strtolower($prop));
      if (method_exists($this, $set_method_name)) {
        $this->{$set_method_name}($elem);
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getRegistry(): OrganisationRegisterInterface {
    try {
      return $this->getOrganisationManager()->getRegistry($this->register);
    }
    catch(PluginNotFoundException $e) {
      throw new RegisterException($e->getMessage(), $e->getCode());
    }
  }

  /**
   * {@inheritDoc}
   */
  public function process($key, $value): mixed {
    return $this->getRegistry()->process($key, $value);
  }

  /**
   * {@inheritDoc}
   */
  public function getId(bool $process = FALSE): ?string {
    return $process ? $this->process('id', $this->id) : $this->id;
  }

  /**
   * {@inheritDoc}
   */
  public function getName(bool $process = FALSE): ?string {
    return $process ? $this->process('name', $this->name) : $this->name;
  }

  /**
   * {@inheritDoc}
   */
  public function getType(bool $process = FALSE): ?string {
    return $process ? $this->process('type', $this->type) : $this->type;
  }

  /**
   * {@inheritDoc}
   */
  public function getStatus(bool $process = FALSE): ?string {
    return $process ? $this->process('status', $this->status) : $this->status;
  }

  /**
   * {@inheritDoc}
   */
  public function getClassification(bool $process = FALSE): ?array {
    return $process ? $this->process('classification', $this->classification) : $this->classification;
  }

  /**
   * {@inheritDoc}
   */
  public function getData(bool $process = FALSE): array {
    $data = $this->data;

    if ($process) {
      foreach ($data as $key => $elem) {
        $data[$key] = $this->process($key, $elem);
      }
    }

    return $data;
  }

  /**
   * Setter for the self::$id property.
   *
   * @param string $id
   *   The internal id of the organisation.
   */
  protected function setId(string $id) {
    $this->id = (string) $id;
  }

  /**
   * Setter for the self::$name property.
   *
   * @param string $name
   *   The name of the organisation.
   */
  protected function setName(string $name) {
    $this->name = (string) $name;
  }

  /**
   * Setter for the self::$type property.
   *
   * @param string $type
   *   The type of the organisation.
   */
  protected function setType(string $type) {
    $this->type = (string) $type;
  }

  /**
   * Setter for the self::$status property.
   *
   * @param string $status
   *   The status of the organisation.
   */
  protected function setStatus(string $status) {
    $this->status = (string) $status;
  }

  /**
   * Setter for the classification properties.
   *
   * @param array $classification
   *   The classification data (optional), should be an
   *   array containing ['codes': array, 'system': string].
   */
  protected function setClassification(array $classification) {
    if (!empty($classification['codes']) && is_array($classification['codes'])
      && !empty($classification['system']) && is_string($classification['system'])) {
        $this->classification = $classification;
    }
  }

}
