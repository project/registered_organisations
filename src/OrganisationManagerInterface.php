<?php

namespace Drupal\registered_organisations;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;

/**
 * Interface for Organisation Manager Services.
 */
interface OrganisationManagerInterface {

  /**
   * Constructor.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler);

  /**
   * Get the register.
   *
   * @param string $register
   *   The plugin id of the register to use.
   *
   * @throws PluginNotFoundException
   *   Thrown if the register does not exist.
   *
   * @return ?OrganisationRegisterInterface
   *   The register plugin.
   */
  public function getRegistry(string $register, array $configuration = []): ?OrganisationRegisterInterface;

  /**
   * Lookup an organisation given its unique identifier.
   *
   * @param string $register
   *   The register to use.
   * @param string $id
   *   The ID for the organisation.
   *
   * @throws DataException
   *   Thrown if there is an error with the data, or with the parameters given.
   * @throws RegisterException
   *   Thrown if there is an error with the data, or with the parameters given.
   * @throws TemporaryException
   *   Thrown if there is a temporary error that should be resolved by retrying.
   *
   * @return mixed
   *   A company record.
   */
  public function lookupOrganisation(string $register, string $id): mixed;

  /**
   * Search for an organisation by name or term.
   *
   * @param string $register
   *   The register to use.
   * @param string $term
   *   The organisation name to search for.
   *
   * @return OrganisationInterface[]
   *   A company record.
   */
  public function searchOrganisation(string $register, string $term): array;

}
