<?php

namespace Drupal\registered_organisations\Plugin\OrganisationRegister;

use Drupal\registered_organisations\DataException;
use Drupal\registered_organisations\TemporaryException;
use Drupal\registered_organisations\RegisterException;
use Drupal\registered_organisations\Organisation;
use Drupal\registered_organisations\OrganisationInterface;
use Drupal\registered_organisations\OrganisationRegisterApi;
use Drupal\registered_organisations\OrganisationRegisterInterface;
use UKGovernmentBEIS\CompaniesHouse\Client as CompaniesClient;
use UKGovernmentBEIS\CompaniesHouse\ApiException;
use UKGovernmentBEIS\CompaniesHouse\NotFoundException;
use UKGovernmentBEIS\CompaniesHouse\RateLimitException;
use UKGovernmentBEIS\CompaniesHouse\UnauthorisedException;

/**
 * Companies House organisation register.
 *
 * @OrganisationRegister(
 *   id = "companies_house",
 *   label = @Translation("Companies House"),
 *   description = @Translation("UK companies registered at Companies House."),
 *   url = "https://www.gov.uk/government/organisations/companies-house",
 *   search_url = "https://find-and-update.company-information.service.gov.uk/advanced-search"
 * )
 */
class CompaniesHouseRegister extends OrganisationRegisterApi {

  /**
   * The organisation type enum.
   *
   * @see https://github.com/companieshouse/api-enumerations/blob/master/constants.yml
   */
  const ORGANISATION_TYPE = [
    'private-unlimited' => "Private unlimited company",
    'ltd' => "Private limited company",
    'plc' => "Public limited company",
    'old-public-company' => "Old public company",
    'private-limited-guarant-nsc-limited-exemption' => "Private Limited Company by guarantee without share capital, use of 'Limited' exemption",
    'limited-partnership' => "Limited partnership",
    'private-limited-guarant-nsc' => "Private limited by guarantee without share capital",
    'converted-or-closed' => "Converted / closed",
    'private-unlimited-nsc' => "Private unlimited company without share capital",
    'private-limited-shares-section-30-exemption' => "Private Limited Company, use of 'Limited' exemption",
    'protected-cell-company' => "Protected cell company",
    'assurance-company' => "Assurance company",
    'oversea-company' => "Overseas company",
    'eeig-establishment' => "European Economic Interest Grouping Establishment (EEIG)",
    'icvc-securities' => "Investment company with variable capital",
    'icvc-warrant' => "Investment company with variable capital",
    'icvc-umbrella' => "Investment company with variable capital",
    'registered-society-non-jurisdictional' => "Registered society",
    'industrial-and-provident-society' => "Industrial and Provident society",
    'northern-ireland' => "Northern Ireland company",
    'northern-ireland-other' => "Credit union (Northern Ireland)",
    'llp' => "Limited liability partnership",
    'royal-charter' => "Royal charter company",
    'investment-company-with-variable-capital' => "Investment company with variable capital",
    'unregistered-company' => "Unregistered company",
    'other' => "Other company type",
    'european-public-limited-liability-company-se' => "European public limited liability company (SE)",
    'united-kingdom-societas' => "United Kingdom Societas",
    'uk-establishment' => "UK establishment company",
    'scottish-partnership' => "Scottish qualifying partnership",
    'charitable-incorporated-organisation' => "Charitable incorporated organisation",
    'scottish-charitable-incorporated-organisation' => "Scottish charitable incorporated organisation",
    'further-education-or-sixth-form-college-corporation' => "Further education or sixth form college corporation",
    'eeig' => "European Economic Interest Grouping (EEIG)",
    'ukeig' => "United Kingdom Economic Interest Grouping",
    'registered-overseas-entity' => "Overseas entity",
  ];

  /**
   * The status enum.
   *
   * @see https://github.com/companieshouse/api-enumerations/blob/master/constants.yml
   */
  const ORGANISATION_STATUS = [
    'active' => "Active",
    'dissolved' => "Dissolved",
    'liquidation' => "Liquidation",
    'receivership' => "Receiver Action",
    'converted-closed' => "Converted / Closed",
    'voluntary-arrangement' => "Voluntary Arrangement",
    'insolvency-proceedings' => "Insolvency Proceedings",
    'administration' => "In Administration",
    'open' => "Open",
    'closed' => "Closed",
    'registered' => "Registered",
    'removed' => "Removed",
  ];

  /**
   * {@inheritDoc}
   */
  public function getClient(): mixed {
    // If the client has already been created.
    if ($this->client) {
      return $this->client;
    }

    $config = \Drupal::config('registered_organisations.settings');
    $api_key = $config->get('companies_house_api_key');

    // An API key must be provided to access these features.
    if (empty($api_key)) {
      throw new RegisterException("No API key was found.");
    }

    // If rate limiting has been hit wait for 5 minutes
    // before allowing requests to be retried.
    $cid = implode(':', [$this->getCachePrefix(), 'rate_limit']);
    if ($this->getCache()->get($cid)) {
      throw new TemporaryException("Rate limit has been hit, please wait before retrying this operation.", 429);
    }

    try {
      $this->client = new CompaniesClient($api_key);
    }
    catch (\Exception $e) {
      throw new RegisterException($e->getMessage());
    }

    return $this->client;
  }

  /**
   * {@inheritDoc}
   *
   * @see https://gist.github.com/rob-murray/01d43581114a6b319034732bcbda29e1#file-uk-company-number-regex
   */
  public function isValidId(string $id): bool {
    $pattern = "/^((AC|ZC|FC|GE|LP|OC|SE|SA|SZ|SF|GS|SL|SO|SC|ES|NA|NZ|NF|GN|NL|NC|R0|NI|EN|\d{2}|SG|FE)\d{5}(\d|C|R))|((RS|SO)\d{3}(\d{3}|\d{2}[WSRCZF]|\d(FI|RS|SA|IP|US|EN|AS)|CUS))|((NI|SL)\d{5}[\dA])|(OC(([\dP]{5}[CWERTB])|([\dP]{4}(OC|CU))))$/";
    return parent::isValidId($id) && preg_match($pattern, $id) === 1;
  }

  /**
   * {@inheritdoc}
   */
  public function process(string $key, mixed $value): mixed {
    // Convert the type and status to a readable format.
    switch ($key) {
      case 'type':
        return self::ORGANISATION_TYPE[$value] ?? $value;

        break;

      case 'status':
        return self::ORGANISATION_STATUS[$value] ?? $value;

        break;
    }

    return parent::process($key, $value);
  }

  /**
   * {@inheritDoc}
   */
  public function getOrganisation(string $id): ?OrganisationInterface {
    // Request the company profile.
    try {
      $response = $this->getClient()?->companyProfile($id);
    }
    catch (NotFoundException $e) {
      throw new DataException($e->getMessage(), $e->getCode());
    }
    catch (RateLimitException $e) {
      // Record the fact that rate limiting has been reached.
      $this->setRateLimitBuffer();
      throw new TemporaryException($e->getMessage(), $e->getCode(), 300);
    }
    catch (ApiException|UnauthorisedException $e) {
      throw new RegisterException($e->getMessage(), $e->getCode());
    }

    // Handle incomplete data.
    if (!$response['company_number'] || !$response['company_name']) {
      throw new DataException("Invalid data returned by the register.", 500);
    }

    // Set the mandatory properties.
    $response += [
      'id' => $response['company_number'],
      'number' => $response['company_number'],
      'name' => $response['company_name'],
      'status' => $response['company_status'],
      'classification' => [
        'codes' => $response['sic_codes'] ?? [],
        'system' => 'SIC',
      ],
      'uri' => "http://data.companieshouse.gov.uk/doc/company/{$response['company_number']}",
    ];

    return new Organisation($this, $response);
  }

  /**
   * {@inheritDoc}
   */
  public function findOrganisation(string $name): array {
    // Request the company profile.
    try {
      $response = $this->getClient()?->searchCompanies($name, NULL, NULL, 'active-companies');
    }
    catch (NotFoundException $e) {
      throw new DataException($e->getMessage(), $e->getCode());
    }
    catch (RateLimitException $e) {
      // Record the fact that rate limiting has been reached.
      $this->setRateLimitBuffer();
      throw new TemporaryException($e->getMessage(), $e->getCode(), 300);
    }
    catch (ApiException|UnauthorisedException $e) {
      throw new RegisterException($e->getMessage(), $e->getCode());
    }

    // Create array of organisation profiles from the results.
    $companies = [];
    foreach ($response["items"] as $search_item) {
      $search_item += [
        'type' => $search_item['company_type'],
        'id' => $search_item['company_number'],
        'name' => $search_item['title'],
        'status' => $search_item['company_status'],
      ];
      $companies[$search_item['company_number']] = new Organisation($this, $search_item);
    }

    return $companies;
  }

}
