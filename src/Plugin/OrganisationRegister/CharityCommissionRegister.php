<?php

namespace Drupal\registered_organisations\Plugin\OrganisationRegister;

use Drupal\Core\Http\ClientFactory;
use Drupal\registered_organisations\DataException;
use Drupal\registered_organisations\RegisterException;
use Drupal\registered_organisations\Organisation;
use Drupal\registered_organisations\OrganisationInterface;
use Drupal\registered_organisations\OrganisationRegisterApi;
use Drupal\registered_organisations\OrganisationRegisterInterface;
use Drupal\registered_organisations\TemporaryException;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;

/**
 * Charity Commission organisation register.
 *
 * @OrganisationRegister(
 *   id = "charity_commission",
 *   label = @Translation("Charity Commission"),
 *   description = @Translation("UK charities registered with the Charity Commission."),
 *   url = "https://www.gov.uk/government/organisations/charity-commission",
 *   search_url = "https://register-of-charities.charitycommission.gov.uk/charity-search"
 * )
 */
class CharityCommissionRegister extends OrganisationRegisterApi {

  /**
   * The organisation type enum.
   */
  const ORGANISATION_TYPE = [
    'Charitable company' => 'Charitable company',
    'CIO' => 'CIO',
    'Other' => 'Other',
    'Trust' => 'Trust',
  ];

  /**
   * The organisation status enum.
   */
  const ORGANISATION_STATUS = [
    'R' => "Registered",
    'RM' => "Removed",
  ];

  /**
   * The api base URI.
   */
  protected string $base = 'https://api.charitycommission.gov.uk/register/api/';

  /**
   * {@inheritDoc}
   */
  public function getClient(): mixed {
    // If the client has already been created.
    if ($this->client) {
      return $this->client;
    }

    $config = \Drupal::config('registered_organisations.settings');
    $api_key = $config->get('charity_commission_api_key');

    // An API key must be provided to access these features.
    if (empty($api_key)) {
      throw new RegisterException("No API key was found.");
    }

    // If rate limiting has been hit wait for 5 minutes
    // before allowing requests to be retried.
    $cid = implode(':', [$this->getCachePrefix(), 'rate_limit']);
    if ($this->getCache()->get($cid)) {
      throw new TemporaryException("Rate limit has been hit, please wait before retrying this operation.", 429);
    }

    try {
      /* @var ClientFactory $factory */
      $http_client = \Drupal::service('http_client_factory');
      $this->client =  $http_client->fromOptions([
        'base_uri' => $this->base,
        'headers' => [
          'Ocp-Apim-Subscription-Key' => $api_key,
        ],
      ]);
    }
    catch (\Exception $e) {
      throw new RegisterException("Failed to create Charity Commission API client");
    }

    return $this->client;
  }

  /**
   * {@inheritDoc}
   *
   * @see https://foodstandardsagency.github.io/enterprise-data-models/patterns/charity_commission_number.html
   */
  public function isValidId(string $id): bool {
    $pattern = "/^\d{7,8}$/";
    return parent::isValidId($id) && preg_match($pattern, $id) === 1;
  }

  /**
   * {@inheritdoc}
   */
  public function process(string $key, mixed $value): mixed {
    // Convert the type and status to a readable format.
    switch ($key) {
      case 'type':
        return self::ORGANISATION_TYPE[$value] ?? $value;

        break;

      case 'status':
        return self::ORGANISATION_STATUS[$value] ?? $value;

        break;
    }

    return parent::process($key, $value);
  }

  /**
   * {@inheritDoc}
   */
  public function getOrganisation(string $id): ?OrganisationInterface {
    try {
      $response = $this->getClient()->get('charitydetails/' . $id . '/0');
      $result = json_decode($response->getBody());
    }
    catch (\Throwable $e) {
      $this->handleException($e);
      return NULL;
    }

    // Construct the organisation profile object.
    $charity_information = [
      'type' => $result->charity_type,
      'id' => $result->reg_charity_number,
      'name' => $result->charity_name,
      'status' => $result->reg_status,
    ];
    // If the charity has a registered company then record this also.
    if ($result->charity_co_reg_number) {
      $charity_information['company'] = $result->charity_co_reg_number;
    }
    return new Organisation($this, $charity_information);
  }

  /**
   * {@inheritDoc}
   */
  public function findOrganisation(string $name): array {
    $organisations = [];

    try {
      $response = $this->getClient()->get('searchCharityName' . '/' . $name);
      $results = json_decode($response->getBody());
    }
    catch (\Throwable $e) {
      $this->handleException($e);
      return $organisations;
    }

    // Create an organisation profile for each result.
    $organisations = [];
    foreach ($results as $result) {
      // If requested filter out inactive charities.
      if ($result->reg_status != 'R') {
        continue;
      }

       $charity_information = [
        'type' => 'charity',
        'id' => $result->reg_charity_number,
        'name' => $result->charity_name,
        'status' => $result->reg_status,
      ];
      $organisations[$result->reg_charity_number] = new Organisation($this, $charity_information);

      // Limit results to 20 items.
      if (count($organisations) >= 20) {
        break;
      }
    }

    return $organisations;
  }

  /**
   * {@inheritdoc}
   */
  private function handleException(\Throwable $e) {
    switch (TRUE) {
      case $e instanceof ClientException:

        // Nothing found.
        if ($e->getCode() == 404) {
          throw new DataException("Charity information could not be found.");
        }

        // Rate limit exceeded.
        if ($e->getCode() == 429) {

          // Create lock record in cache.
          $cid = implode(':', [$this->getCachePrefix(), 'rate_limit']);
          $expiry = $this->getRequestTime() + OrganisationRegisterInterface::RATE_LIMIT_BUFFER;
          $this->getCache()->set($cid, $this->getRequestTime(), $expiry);

          throw new TemporaryException("Too many requests, rate limit reached.", $e->getCode());
        }

        throw new RegisterException("Charity Commission API Client exception encountered: @message", $e->getCode());

      default:
        throw new RegisterException("Charity Commission API exception encountered: @message", $e->getCode());
    }
  }

}
