<?php

namespace Drupal\registered_organisations;

/**
 * Interface for Organisation Register Plugin.
 */
interface OrganisationRegisterInterface {

  /**
   * Cache prefix.
   *
   * @var string
   */
  const CACHE_PREFIX = 'registered_organisations';

  /**
   * The rate limit buffer in seconds.
   *
   * If rate limiting is hit by any of the registers pause requests for the
   * specified amount of time.
   *
   * @var int
   */
  const RATE_LIMIT_BUFFER = 60;

  /**
   * Get the plugin label
   *
   * @return string
   *   The name of the plugin.
   */
  public function getLabel(): string;

  /**
   * Check whether the plugin can connect to the external register.
   *
   * @return bool
   *   True if the register can be connected to.
   */
  public function checkConnection(): bool;

  /**
   * Get the client or service required for connecting to the external register.
   *
   * If a client is returned it is assumed that a connection can be attempted,
   * before returning the client ensure that it can open a valid connection.
   *
   * @throws \Drupal\registered_organisations\RegisterException
   *   If a connection cannot be opened.
   *
   * @return mixed
   *   A client / service for connecting to the register.
   */
  public function getClient(): mixed;

  /**
   * Check whether the id is suitable for this register plugin.
   *
   * Allows filtering based on the id value before calls are made to
   * look up the organisation from the register.
   *
   * @param string $id
   *   The ID to validate.
   *
   * @return bool
   */
  public function isValidId(string $id): bool;

  /**
   * Get an organisation by ID.
   *
   * @param string $id
   *   An organisation ID to be looked up.
   *
   * @throws RegisterException
   *   If the register could not be retrieved or accessed.
   * @throws TemporaryException
   *   In the event of a temporary failure that should be re-attempted.
   * @throws DataException
   *   If there are errors with the data that is retrieved.
   *
   * @return ?OrganisationInterface
   *   An organisation profile object.
   */
  public function getOrganisation(string $id): ?OrganisationInterface;

  /**
   * Find an organisation by name.
   *
   * @param string $name
   *   An organisation name to be looked up.
   *
   * @throws RegisterException
   *   If the register could not be retrieved or accessed.
   * @throws TemporaryException
   *   In the event of a temporary failure that should be re-attempted.
   * @throws DataException
   *   If there are errors with the data that is retrieved.
   *
   * @return OrganisationInterface[]
   *   An array of matched organisation profile objects.
   */
  public function findOrganisation(string $name): array;

  /**
   * Process the data from any raw input to a format suitable for output.
   *
   * @param string $key
   *   This element's key within the data array.
   * @param mixed $value
   *   The data value to process.
   *
   * @return mixed
   */
  public function process(string $key, mixed $value): mixed;

}
