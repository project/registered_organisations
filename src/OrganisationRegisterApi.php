<?php

namespace Drupal\registered_organisations;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Cache\CacheBackendInterface;
use Psr\Log\LoggerInterface;

/**
 * Provides a base implementation for an OrganisationRegister plugin.
 *
 * @see plugin_api
 */
abstract class OrganisationRegisterApi extends PluginBase implements OrganisationRegisterInterface {

  /**
   * An optional php client for handling api requests.
   *
   * @var mixed
   */
  protected mixed $client = NULL;

  /**
   * Constructs a \Drupal\Component\Plugin\PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    $this->configuration = $configuration;
    $this->pluginId = $plugin_id;
    $this->pluginDefinition = $plugin_definition;
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel(): string {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritDoc}
   */
  public function checkConnection(): bool {
    try {
      $this->getClient();
    }
    catch (RegisterException $e) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function isValidId(string $id): bool {
    return !empty($id);
  }

  /**
   * {@inheritDoc}
   */
  public function process(string $key, mixed $value): mixed {
    return $value;
  }


  /**
   * Get a cache client for storing request information.
   *
   * @return \Drupal\Core\Cache\CacheBackendInterface
   *   The cache bin to use.
   */
  protected function getCache(): CacheBackendInterface {
    return \Drupal::cache();
  }

  /**
   * Get the cache prefix.
   */
  protected function getCachePrefix(): string {
    return implode(':', [
      OrganisationRegisterInterface::CACHE_PREFIX,
      $this->getPluginId(),
    ]);
  }

  /**
   * Set rate limit buffer.
   *
   * Allow requests to a register to be paused for a specified amount of time.
   */
  public function setRateLimitBuffer() {
    $cid = implode(':', [$this->getCachePrefix(), 'rate_limit']);
    $expiry = $this->getRequestTime() + OrganisationRegisterInterface::RATE_LIMIT_BUFFER;
    $this->getCache()->set($cid, $this->getRequestTime(), $expiry);
  }

  /**
   * Get the request time.
   */
  protected function getRequestTime() {
    return \Drupal::time()->getRequestTime();
  }

}
