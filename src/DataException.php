<?php

namespace Drupal\registered_organisations;

/**
 * An exception thrown when issues are found with the data retrieved
 * from a register.
 */
class DataException extends \Exception {

}
