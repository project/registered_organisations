<?php

namespace Drupal\registered_organisations;

/**
 * Interface for Organisation Register Plugin.
 */
interface OrganisationInterface {

  /**
   * Get the register plugin used to retrieve this company record
   *
   * @throws RegisterException
   *   In the event of an invalid register ID.
   *
   * @return OrganisationRegisterInterface
   *   The register plugin.
   */
  public function getRegistry(): OrganisationRegisterInterface;

  /**
   * Allows the register plugin to process the values for output.
   *
   * This transforms the data from internal values stored in
   * the register's backed, into user readable values.
   *
   * @return OrganisationRegisterInterface
   *   The register plugin.
   */
  public function process(string $key, mixed $value): mixed;

  /**
   * Get the unique organisation id/number recorded in the register.
   *
   * @param bool $process
   *   Whether to process the value for output.
   *
   * @return ?string
   *   The ID or organisation number.
   */
  public function getId(bool $process = FALSE): ?string;

  /**
   * Get the organisation name.
   *
   * @param bool $process
   *   Whether to process the value for output.
   *
   * @return ?string
   *   The organisation name.
   */
  public function getName(bool $process = FALSE): ?string;

  /**
   * Get the organisation type.
   *
   * @param bool $process
   *   Whether to process the value for output.
   *
   * @return ?string
   *   The organisation type.
   */
  public function getType(bool $process = FALSE): ?string;

  /**
   * Get the organisation status.
   *
   * @param bool $process
   *   Whether to process the value for output.
   *
   * @return ?string
   *   The organisation status.
   */
  public function getStatus(bool $process = FALSE): ?string;

  /**
   * Get the organisation's industry classification.
   *
   * @param bool $process
   *   Whether to process the value for output.
   *
   * @return ?array
   *   The organisation's industry classification,
   *   An array containing the keys 'codes' & 'system'.
   *   Where 'system' represents one of the established
   *   industry classification systems: SIC, NACE, NAICS.
   */
  public function getClassification(bool $process = FALSE): ?array;

  /**
   * Get the raw data array as passed created by the calling plugin.
   *
   * @param bool $process
   *   Whether to process the value for output.
   *
   * @return array
   *   The raw data containing additional organisation information.
   */
  public function getData(bool $process = FALSE): array;

}
