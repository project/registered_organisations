<?php

namespace Drupal\registered_organisations;

use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Psr\Log\LoggerInterface;

/**
 * Service class for GovUK Notify.
 */
class OrganisationManager extends DefaultPluginManager implements OrganisationManagerInterface {

  /**
   * Constructs an Organisation object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
          'Plugin/OrganisationRegister',
          $namespaces,
          $module_handler,
          'Drupal\registered_organisations\OrganisationRegisterInterface',
          'Drupal\registered_organisations\Annotation\OrganisationRegister'
      );

    $this->alterInfo('registered_organisations_info');
    $this->setCacheBackend($cache_backend, 'registered_organisations_info_plugins');
    $this->factory = new DefaultFactory($this->getDiscovery());
  }

  /**
   * Returns the logger instance.
   *
   * @return LoggerInterface
   *   The logger.
   */
  public function getLogger(): LoggerInterface {
    return \Drupal::logger('registered_organisations');
  }

  /**
   * {@inheritdoc}
   */
  public function getRegistry(string $register, array $configuration = []): ?OrganisationRegisterInterface {
    return $this->createInstance($register, $configuration);
  }

  /**
   * {@inheritDoc}
   */
  public function lookupOrganisation(string $register, string $id): mixed {
    $profile = &drupal_static(__FUNCTION__ . ':' . $register . ':' . $id);
    if (isset($profile)) {
      return $profile;
    }

    try {
      // Get the register plugin.
      $registry = $this->getRegistry($register);

      // Ensure that the id is valid for the register before looking it up.
      if (!$registry?->isValidId($id)) {
        throw new DataException("Invalid ID ($id) submitted to the register", 401);
      }

      return $registry?->getOrganisation($id);
    }
    catch (RegisterException|PluginNotFoundException $ignored) {
      // Do not pass on failures to load the register.
      return NULL;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function searchOrganisation(string $register, string $term): array {
    $search = &drupal_static(__FUNCTION__ . ':' . $register . ':' . $term);
    if (isset($search)) {
      return $search;
    }

    try {
      $register = $this->getRegistry($register);
      return $register?->findOrganisation($term);
    }
    catch (RegisterException|PluginNotFoundException $ignored) {
      // Do not pass on failures to load the register.
      return [];
    }
  }

}
