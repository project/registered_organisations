## Registered Organisations

This module provides an interface for accessing organisation data from public registers.

### Registered Organisation Plugins

Plugins can be provided to connect to additional organisation registers. Provided by default are:
- Companies House (UK)
- Charity Commission (UK)

API Keys for these services can be provided through the config system.
```
$config['registered_organisations.settings']['companies_house_api_key'] = 'PRIVATE_API_KEY';
$config['registered_organisations.settings']['charity_commission_api_key'] = 'PRIVATE_API_KEY';
```
